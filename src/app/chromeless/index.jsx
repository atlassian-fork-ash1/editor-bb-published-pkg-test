import React from "react";
import {render} from "react-dom";
import "babel-polyfill";
import ChromelessEditor from "../component/ChromelessEditor.jsx";

class App extends React.Component {
  render () {
    return (
      <div>
        <p> Edit here!</p>
        <ChromelessEditor />
      </div>
    );
  }
}


render(<App/>, document.getElementById("app"));
