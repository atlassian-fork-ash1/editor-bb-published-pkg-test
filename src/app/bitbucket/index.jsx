import React from "react";
import {render} from "react-dom";
import "babel-polyfill";
import CommentEditor from "../component/CommentEditor.jsx";

class App extends React.Component {
  render () {
    return (
      <div>
        <p> Edit here!</p>
        <CommentEditor />
      </div>
    );
  }
}


render(<App/>, document.getElementById("app"));
