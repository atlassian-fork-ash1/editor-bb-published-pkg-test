import React from "react";
import { Editor, CollapsedEditor, WithEditorActions, EditorContext } from "@atlaskit/editor-core";
import { BitbucketTransformer } from "@atlaskit/editor-bitbucket-transformer";
import { storyData as emojiStoryData } from "@atlaskit/emoji/dist/es5/support";
import mockMentionProvider from "../bitbucket/resources/mock-mention-provider";

class BitBucketEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      markdown: "markdown"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleSave (editorActions) {
    editorActions.getValue().then(value => {
      alert(`Saved with python-markdown value: ${value}`);
    });
  }

  handleCancel(editorActions) {
    editorActions.clear();
  }

  handleChange (editorActions) {
    editorActions.getValue().then(value => {
      this.setState({
        markdown: value
      });
    });
  }


  render() {
    const { markdown } = this.state;
    const mentionProvider = mockMentionProvider.get();
    return (
      <div>
        <EditorContext>
          <WithEditorActions
            render={actions => (
              <CollapsedEditor placeholder="Test editor" isExpanded>
                <Editor
                  appearance="comment"
                  allowCodeBlocks={true}
                  allowHyperlinks={true}
                  allowLists={true}
                  allowMentions={true}
                  allowTextColor={true}
                  allowTextFormatting={true}
                  allowTables={true}

                  emojiProvider={Promise.resolve(emojiStoryData.getEmojiResource({ uploadSupported: true }))}
                  mentionProvider={mentionProvider}

                  contentTransformerProvider={schema => new BitbucketTransformer(schema)}

                  onCancel={() => this.handleCancel(actions)}
                  onChange={() => this.handleChange(actions)}
                  onSave={() => this.handleSave(actions)}
                />
              </CollapsedEditor>
            )}
          />
        </EditorContext>
        <pre id="markdown">{markdown}</pre>
      </div>
    );
  }
}

export default BitBucketEditor;