import React from "react";
import {render} from "react-dom";
import "babel-polyfill";
import CoreEditor from "../component/MessageEditor.jsx";

class App extends React.Component {
  render () {
    return (
      <div>
        <p> Edit here!</p>
        <CoreEditor />
      </div>
    );
  }
}


render(<App/>, document.getElementById("app"));
