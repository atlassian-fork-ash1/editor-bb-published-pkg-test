import EditorPage from "./pages/editor.page";
import { expect }from "chai";

describe("actionsAndDecisions", function () {
  this.retries(2);
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });

  it("creates an action", function () {
    const actionAutoFormat = "[] ";
    const actionElementSelector = "ol[data-task-list-local-id]";
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue([actionAutoFormat,"a new task"]);
    EditorPage.waitForElement(actionElementSelector);
    expect(EditorPage.getText(actionElementSelector).should.contain("a new task"));
  });

  it("creates an decision", function () {
    const decisionAutoFormat = "<> ";
    const decisionElementSelector = "ol[data-decision-list-local-id]";
  
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue([decisionAutoFormat," a new decision"]);
    EditorPage.waitForElement(decisionElementSelector);
    expect(EditorPage.getText(decisionElementSelector).should.contain("a new decision"));
  });

  it.only("converts an action to decision", function () {
    const decisionAutoFormat = "<> ";
    const decisionElementSelector = "ol[data-decision-list-local-id]";
    const taskElementSelector = "ol[data-task-list-local-id]";
    const taskButton = "[aria-label=\"Create action\"]";
  
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.setValue([decisionAutoFormat," a new decision "]);
    EditorPage.waitForElement(decisionElementSelector);

    EditorPage.click(taskButton);

    EditorPage.waitForElement(taskElementSelector);
    expect(EditorPage.getText(taskElementSelector).should.contain("a new decision"));
  });
});