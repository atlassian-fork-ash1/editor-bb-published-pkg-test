import EditorPage from "./pages/editor.page";

describe( "mentions:" , function() {
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });

  this.retries(2);
  it ("shows mention picker", function(){
    browser.waitForExist("[contenteditable=\"true\"]");
    EditorPage.editable.setValue(" ");
    EditorPage.editable.addValue("@");
    EditorPage.waitForElement(EditorPage.mentionPicker);
    expect(EditorPage.contains ("[data-mention-query=\"true\"]").should.be.equal(true));
    expect(EditorPage.contains( EditorPage.mentionPicker).should.be.equal(true));
  });
  it ("user is selected on firstname only if one user is available", function(){
    EditorPage.editable.setValue("@Raina");
    EditorPage.waitForElement(EditorPage.mentionPicker);
    EditorPage.contains(EditorPage.mentionPicker).should.be.equal(true);
    EditorPage.editable.addValue("Enter");
    if(process.env.PKG === "bitbucket"){
      expect(EditorPage.contains("[data-mention-id=\"Caprice\"]").should.be.equal(true));
    }else{
      expect(EditorPage.contains("[data-mention-id=\"0\"]").should.be.equal(true));
    }
  });
  it("user can mention existing user using only mention-name", function(){
    if (!(process.env.PKG === "bitbucket")) {
      EditorPage.editable.setValue([" "]);
      EditorPage.editable.addValue(["@Jasmine"]);
      EditorPage.waitForElement(EditorPage.mentionPicker);
      EditorPage.editable.addValue("Enter");
      expect(EditorPage.contains("span=@Jasmine").should.be.equal(true));
    }
  });
  it("mention disappears when user does not exist",function(){
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue("@barb");
    EditorPage.contains(".ak-mention-picker").should.be.equal(true);
    EditorPage.editable.addValue([" a"]);
    if (browser.desiredCapabilities.browserName.includes("IE"))
      expect(EditorPage.contains("[data-mention-id]").should.be.equal(false));
    else
      expect(EditorPage.contains(".ak-mention-picker").should.be.equal(false));
  });
  it.skip("mention stays on username space if more than one user",function(){
    EditorPage.editable.setValue("@Alica");
    EditorPage.editable.addValue("Space");
    EditorPage.editable.addValue("Woods");
    EditorPage.editable.addValue("Space");
    expect(EditorPage.contains( "span=@Alica Woods").should.be.equal(true));
  });
  it("mention disappears on escape", function(){
    EditorPage.editable.setValue([" "]);
    EditorPage.editable.addValue(["@","Fre"])
    EditorPage.waitForElement(EditorPage.mentionPicker);
    EditorPage.editable.addValue("Escape");
    expect(EditorPage.contains( EditorPage.mentionPicker).should.be.equal(false));
  });
});