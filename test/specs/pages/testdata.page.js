import Page from "./page";

class TestdataPage extends Page {
  
  openTestdata() {
    browser.url("http://localhost:8080/testdata/index.html");
  }
  
  copyPlainText(){
    browser.click("#copy-plain-text").waitForText("#plain-copy-result");
  }
  
  copyTestData(){
    browser.click("#markup-dom-copy").waitForText("#markup-dom-copy-result");
  }
  
  copyTestTable(){
    browser.click("#markup-table-copy").waitForText("#markup-table-copy-result");
  }
  
  copyList(){
    browser.click("#markup-ul-copy").waitForText("#markup-ul-copy-result");
  }
  
  copyOrderedList(){
    browser.click("#markup-ol-copy").waitForText("#markup-ol-copy-result");
  }
}

export default new TestdataPage();
