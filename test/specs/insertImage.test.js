import EditorPage from "./pages/editor.page";
import path from "path";

describe("upload", function () {
  let toUpload;
  const inputFile = "input[type=\"file\"]";
  const textInput = "hi there!";
  const image = ".img-wrapper";
  const mediacard = ".media-card";
  const number = 5;   
  const list = "li";
  const ul = "ul";

  this.retries(2); 
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
    toUpload = browser.desiredCapabilities.os === "Windows" ? "C:\\Users\\hello\\Desktop\\images\\wallpaper4.jpg" : path.join(__dirname, "fun_zone.jpg");
  });

  
  it("should upload file and add some text", function () {    
    EditorPage.waitForElement(inputFile);
    EditorPage.fileUpload.addValue(toUpload);
    
    EditorPage.waitForElement(image);
    expect(browser.isExisting(image).should.be.true);
    expect(browser.isExisting(mediacard).should.be.true);

    EditorPage.editable.addValue(textInput);    
    expect(EditorPage.editable.getText().should.include(textInput));   
  });

  it("should be able to remove files",function(){
    const remove = "[aria-label=\"delete\"]";
    EditorPage.click(remove);

    expect(EditorPage.contains(image).should.be.false);
  });

  it("should upload multiple files", function () { 

    EditorPage.waitForElement(inputFile);
    EditorPage.editable.setValue(textInput);
    for (let i = 0;i < number; i++){
        EditorPage.fileUpload.addValue(toUpload);
    }  
    EditorPage.waitForElement(image);
    EditorPage.waitForElement(ul);
    expect(EditorPage.contains(ul).should.be.true);
    
    expect(EditorPage.contains(image).should.be.true);
    expect(EditorPage.contains(mediacard).should.be.true);
    expect(browser.elements(list).value.length.should.equal(number));

    EditorPage.editable.addValue(textInput);    
    expect(EditorPage.editable.getText().should.include(textInput));   
  });
});