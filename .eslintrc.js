module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "mocha":true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
        "jsx": true
     },
        "sourceType": "module"
  },
  "globals": {
    "browser": true,
    "expect": true,
    "exports":true,
    "console":true,
    "process":true,
    "module":true
  },
  "plugins": [
    "react"
  ],
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ]
  }
};