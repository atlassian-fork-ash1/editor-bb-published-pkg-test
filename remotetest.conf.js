exports.config = {
  user: process.env.BROWSERSTACK_USERNAME,
  key: process.env.BROWSERSTACK_ACCESS_KEY,

  updateJob: false,
  specs: [
    "./test/specs/*.test.js"
  ],
  exclude: [],
  
  suites: {
    emoji: [
      "./test/specs/emoji.test.js",
    ],
    mention: [
      "./test/specs/mentions.test.js"
    ],
    actionAndDecision: [
      "./test/specs/actionsAndDecisions.test.js"
    ],
    format: [
      "./test/specs/textFormat.test.js"
    ],
    toolbar: [
      "./test/specs/toolbarFormat.test.js",
      "./test/specs/link.test.js",
      "./test/specs/list.test.js",
    ],
    copyPaste: [
      "./test/specs/copyPaste.js"
    ],
    media: [
      "./test/specs/insertImage.test.js"
    ]
  },

  maxInstances: 5,
  maxSessions: 1,
  
  commonCapabilities: {
    project: "PF-Editor smoke test",
    build: process.env.BUILD_NUMBER || "Test1",
    services: ['browserstack'],
    "browserstack.local": true,
    "browserstack.debug": true
  },

  capabilities: [
    // {
    //   "os": "Windows",
    //   "os_version": "10",
    //   "browserName": "IE",
    //   "browser_version": "11.0",
    //   "resolution": "1024x768",
    // },
    {
      "os": "Windows",
      "os_version": "10",
      "browserName": "Chrome",
      "browser_version": "62",
      "resolution": "1024x768",
    },
    {
      "os": "Windows",
      "os_version": "10",
      "browserName": "Firefox",
      "browser_version": "57",
      "resolution": "1024x768"
    }
    // {
    //   "os": "Windows",
    //   "os_version": "10",
    //   "browserName": "Edge",
    //   "browser_version": "15.0",
    //   "resolution": "1024x768",
    // }

  ],
  
  before: function() {
    
    let chai = require("chai");
    global.expect = chai.expect;
    chai.Should();
    let pkg = process.env.PKG || "bitbucket";
    global.baseUrl = "http://localhost:8080/" + pkg;
  },
  // Code to start browserstack local before start of test
  onPrepare: function (config, capabilities) {
    console.log("Connecting local");
    const browserstack = require("browserstack-local");
    return new Promise(function (resolve, reject) {
      exports.bs_local = new browserstack.Local();
      exports.bs_local.start({ "key": exports.config.key }, function (error) {
        if (error) return reject(error);
        console.log("Connected. Now testing...");
        resolve();
      });
    });
  },
  
  beforeSession: function (config, capabilities, specs) {
    let testname = (specs && specs[0].split("/").pop().replace(".test.js", "".trim())) || undefined;
    capabilities.name = `${process.env.PKG}.${testname}`;
  },

  // Code to stop browserstack local after end of test
  onComplete: function (capabilties, specs) {
    exports.bs_local.stop(function () {});
  },

  logLevel: "verbose",
  coloredLogs: true,
  screenshotPath: `./build-output/${process.env.PKG}/errorShots/`,
  waitforTimeout: 90000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,

  framework: "mocha",
  mochaOpts: {
    compilers: ["js:babel-register"],
    timeout: 600000,
    ui: "bdd"
  },

  reporters: ["junit"],
  reporterOptions: {
    junit: {
      outputDir: `./test-reports/${process.env.PKG}/`,
      outputFileFormat: function(opts) { // optional
        return `WDIO.xunit.${opts.capabilities}.${process.env.PKG}.${opts.cid}.xml`;
      }
    }
  }
};

// Code to support common capabilities
exports.config.capabilities.forEach(function(caps){
  for(let i in exports.config.commonCapabilities) caps[i] = caps[i] || exports.config.commonCapabilities[i];
});

